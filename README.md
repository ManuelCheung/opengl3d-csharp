# OpenGL3D-Csharp

OpenGL build 3D model with wpf C#

3D桌面版客户端

根据OpenGL标准，用C#的wpf纯手写3D基本场景，对于复杂的模型借助Maya用Blend转换导出格式再用VisualStudio导入，开发出可交互3D建模场景的客户端软件。
